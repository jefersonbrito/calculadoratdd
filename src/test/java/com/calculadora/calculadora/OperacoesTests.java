package com.calculadora.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTests {

    @Test
    public void testarOperaçãoDeSoma(){
        Assertions.assertEquals(Operacao.soma(1,1),2);
    }

    @Test
    public void testarOperaçãoDeSomaLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.somaLista(numeros),6);
    }

    @Test
    public void testarOperaçãoDeSubtracao(){
        Assertions.assertEquals(Operacao.subtracao(2,1),1);
    }

    @Test
    public void testarOperaçãoDeSubtracaoLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(5);
        Assertions.assertEquals(Operacao.subtracaoLista(numeros), 2);
    }

    @Test
    public void testarOperaçãoDeMultiplicação(){
        Assertions.assertEquals(Operacao.multiplicacao(2,2), 4);
    }

    @Test
    public void testarOperaçãoDeMultiplicaçãoLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(3);
        numeros.add(2);
        numeros.add(5);
        Assertions.assertEquals(Operacao.multiplicacaoLista(numeros), 30);
    }

    @Test
    public void testaOperaçãoDeDivisão(){
        Assertions.assertEquals(Operacao.divisao(4,2), 2);
    }

}
